package test

import java.io.File

import scala.collection.immutable.TreeMap
import scala.io.Source
import scala.util.{Try, Using}

object SimpleSearch extends App {
  Program
    .readFile(args)
    .fold(
      println,
      file => Program.iterate(Program.index(file)))
}

object Program {

  import scala.io.StdIn.readLine

  val CommandQuit = ":quit"

  val StopWords: Seq[String] = Seq("the", "a", "i", "me", "to", "what", "in", "a", "an", "am", "is", "are") // TODO: complete it and put them into file or db

  case class IndexResult(index: Index, errors: Seq[FileReadProblem])

  case class Index(words: Map[String, Seq[Word]])

  case class Word(word: String, filename: String, count: Long) {
    def inc() = Word(word, filename, count + 1)
  }

  case class MatchedFile(filename: String, matchWordCount: Long)

  trait FileReader {
    def getLines(filePath: String): Try[Seq[String]]
  }

  implicit val fileSystemReader: FileReader = filename => Using(Source.fromFile(filename)) {
    _.getLines.toSeq
  }

  sealed trait ReadFileError

  case object MissingPathArg extends ReadFileError

  case class NotDirectory(error: String) extends ReadFileError

  case class FileNotFound(t: Throwable) extends ReadFileError

  case class FileReadProblem(error: String) extends ReadFileError

  def readFile(args: Array[String]): Either[ReadFileError, File] = {
    for {
      path <- args.headOption.toRight(MissingPathArg)
      file <- Try(new java.io.File(path))
        .fold(
          throwable => Left(FileNotFound(throwable)),
          file =>
            if (file.isDirectory) Right(file)
            else Left(NotDirectory(s"Path [$path] is not a directory"))
        )
    } yield file
  }

  def index(directory: File)(implicit fileReader: FileReader): IndexResult = {

    val (errors, words) = directory.listFiles().toSeq
      .filter(_.isFile)
      .map(extract(_, fileReader, StopWords))
      .partitionMap(identity)

    IndexResult(Index(words.flatten.groupMap(_._1)(_._2)), errors)
  }

  def iterate(indexResult: IndexResult): Unit = {
    stringifyErrors(indexResult.errors).foreach(m => println("indexing errors: " + m))
    print(s"search> ")

    readLine() match {
      case CommandQuit => println("goodbye")
      case _@searchQuery =>
        val searchQueryWords = tokenizeSearchQuery(searchQuery, StopWords)
        println(stringifyResult(rank(search(searchQueryWords, indexResult.index), 10), searchQueryWords.size))

        iterate(IndexResult(indexResult.index, Seq.empty))
    }
  }

  def tokenizeSearchQuery(searchQuery: String, stopWords: Seq[String]): Seq[String] = {
    searchQuery.trim.split(" ").toSeq.filter(!stopWords.contains(_))
  }

  def extract(file: File, fileReader: FileReader, stopWords: Seq[String]): Either[FileReadProblem, Map[String, Word]] = {
    fileReader.getLines(file.getPath).fold(
      (_: Throwable) => Left(FileReadProblem(s"File [${file.getPath}] is not readable")),
      lines => Right(lines.flatMap(_.split("\\W+")
        .map(_.toLowerCase())
        .filter(!stopWords.contains(_)))
        .foldLeft(TreeMap.empty[String, Word]) {
          (acc, word) => acc + (word -> (acc.getOrElse(word, Word(word, file.getName, 0)).inc()))
        })
    )
  }

  def stringifyResult(searchResults: Seq[MatchedFile], searchWordCount: Int): String = {
    searchResults
      .map(item => s"${item.filename} : ${Math.ceil(item.matchWordCount.toFloat / searchWordCount * 100).toInt}% ")
      .reduceLeftOption((acc, itemStr) => acc + itemStr)
      .getOrElse("no matches found")
  }

  def rank(words: Seq[Word], returnCount: Int): Seq[MatchedFile] = {
    for {
      word <- words
        .groupMapReduce(_.filename)(_ => 1L)(_ + _)
        .toSeq
        .sortWith(_._2 > _._2)
        .take(returnCount)
    } yield (MatchedFile.apply _).tupled(word)
  }

  def search(searchQueryWords: Seq[String], index: Index): Seq[Word] = {
    (for {
      searchWord <- searchQueryWords
      word <- index.words.get(searchWord.toLowerCase)
    } yield word).flatten
  }

  def stringifyErrors(errors: Seq[FileReadProblem]): Option[String] = {
    (for {
      error <- errors
    } yield error.toString).reduceOption((t1, t2) => t1 + "\n" + t2)
  }
}