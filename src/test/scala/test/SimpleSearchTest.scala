package test

import java.io.File

import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpec, Matchers}
import test.Program.{FileReadProblem, FileReader, Index, MatchedFile, Word}

import scala.collection.immutable.TreeMap
import scala.util.{Failure, Try}

class SimpleSearchTest extends FlatSpec with Matchers with MockFactory {

  class MockDir extends File("foo")

  class MockFile(name: String) extends File(name) {
    override def getName: String = name

    override def getPath: String = name

    override def isFile: Boolean = true
  }

  "extract function" should "successfully count words" in {
    val stringReader: FileReader =
      _ => Try {
        List("I am Saeed", "I am a software developer", "I love to create programs in functional way")
      }

    val result = Program.extract(new File("filename"), stringReader, Program.StopWords)
    val words = result.toOption.get

    words.size shouldBe 8
    words.keys should contain allOf("create", "developer", "functional", "love", "programs", "saeed", "software", "way")

    words("software").count shouldBe 1
    words.contains("i") shouldBe false
  }

  "extract function" should "can not read file" in {
    val stringReader: FileReader =
      _ => Failure(new RuntimeException("fail to open file"))

    val result = Program.extract(new File("filename"), stringReader, Program.StopWords)
    val error = result.swap.toOption.get.toString

    error shouldBe "FileReadProblem(File [filename] is not readable)"
  }

  "index function" should "return cumulative index" in {
    implicit val stringReader: FileReader = {
      case "file1" => Try(List("I am Saeed", "I am a software developer"))
      case "file2" => Try(List("I love create programs in functional way"))
      case "file3" => Try(List("I love Scala more than Haskell !!!", "I am a leaner"))
    }

    val mockDir = stub[MockDir]

    (mockDir.listFiles: () => Array[File]).when().returns(Array(new MockFile("file1"), new MockFile("file2"), new MockFile("file3")))

    val result = Program.index(mockDir)
    val words = result.index.words

    words.size shouldBe 13
    words("love").size shouldBe 2
    words.contains("i") shouldBe false
  }

  "index function" should "return cumulative index and errors" in {
    implicit val stringReader: FileReader = {
      case "file1" => Try(List("I am Saeed", "I am a software developer"))
      case "file2" => Failure(new RuntimeException("fail to open file"))
      case "file3" => Failure(new RuntimeException("corrupted file"))
    }

    val mockDir = stub[MockDir]

    (mockDir.listFiles: () => Array[File]).when().returns(Array(new MockFile("file1"), new MockFile("file2"), new MockFile("file3")))

    val result = Program.index(mockDir)
    val words = result.index.words
    val errors = result.errors.map(_.toString)

    errors.size shouldBe 2
    errors should contain allOf("FileReadProblem(File [file2] is not readable)", "FileReadProblem(File [file3] is not readable)")

    words.size shouldBe 3
    words.contains("am") shouldBe false
  }

  "stringifyResult function" should "convert matched items to string presentation" in {
    val searchResults: Seq[Program.MatchedFile] = Seq(
      MatchedFile("file 1", 2), MatchedFile("file 2", 0), MatchedFile("file 3", 3), MatchedFile("file 4", 1)
    )

    val result = Program.stringifyResult(searchResults, 3)

    result shouldBe "file 1 : 67% file 2 : 0% file 3 : 100% file 4 : 34% "
  }

  "stringifyResult function" should "return no matches found when nothing find" in {
    val searchResults: Seq[Program.MatchedFile] = Seq.empty

    val result = Program.stringifyResult(searchResults, 3)

    result shouldBe "no matches found"
  }

  "tokenizeSearchQuery function" should "tokenize SearchQuery and remove stop words from it" in {
    val errors = Seq(
      FileReadProblem("File [file1] is not readable")
      , FileReadProblem("File [file2] is not readable")
    )

    val result = Program.tokenizeSearchQuery("Scala is a great programming language", Program.StopWords)

    result.size shouldBe 4
    result shouldEqual Seq("Scala", "great", "programming", "language")
  }

  "rank function" should "rank result words and sort them" in {
    val words = Seq(
      Word("scala", "file1", 2), Word("love", "file2", 1), Word("java", "file1", 3), Word("life", "file4", 1)
      , Word("scala", "file8", 1), Word("love", "file5", 5), Word("java", "file7", 1), Word("life", "file10", 10)
      , Word("scala", "file3", 2), Word("love", "file3", 1), Word("java", "file6", 1), Word("life", "file11", 2)
      , Word("scala", "file6", 3), Word("love", "file8", 3), Word("java", "file2", 2), Word("life", "file5", 1)
      , Word("scala", "file7", 3), Word("love", "file9", 3), Word("java", "file3", 2), Word("life", "file6", 1)
    )

    val result = Program.rank(words, 5)

    result.size shouldBe 5
    result shouldEqual Seq(MatchedFile("file6", 3), MatchedFile("file3", 3), MatchedFile("file2", 2), MatchedFile("file1", 2), MatchedFile("file7", 2))
  }

  "search function" should "return a list of words that find them in the Index" in {
    val index = Index(
      TreeMap(
        "scala" -> Seq(Word("scala", "file1", 2), Word("scala", "file2", 1), Word("scala", "file1", 3), Word("scala", "file4", 1))
        , "java" -> Seq(Word("java", "file1", 2), Word("java", "file2", 1), Word("java", "file1", 3))
        , "sweden" -> Seq(Word("sweden", "file1", 2), Word("sweden", "file2", 1), Word("sweden", "file1", 3), Word("sweden", "file4", 1))
        , "love" -> Seq(Word("love", "file1", 2), Word("love", "file2", 1), Word("love", "file1", 3), Word("love", "file4", 1))
        , "nasim" -> Seq(Word("nasim", "file1", 2), Word("nasim", "file2", 1), Word("nasim", "file1", 3), Word("nasim", "file4", 1))
        , "delaram" -> Seq(Word("delaram", "file1", 2))
      )
    )

    val result = Program.search(Seq("Scala", "and", "Java", "in", "Sweden"), index)

    result.size shouldBe 11
    result shouldEqual index.words("scala") ++ index.words("java") ++ index.words("sweden")
  }

}